/** 
Albarran Maria E.
Guillen Elias 
**/

#ifndef TICTACTOE
#define TICTACTOE	

using namespace std;
class Tictactoe{
	
	private:

		char Square[26];
		int turn;
    int mode; //1.jugador vs jugador 2.jugador vs pc

	public: 

		void set_turn(int t){	

			this->turn=t;
		}

		int get_turn(){	return(this->turn);}

		void set_mode(int m){	

			this->mode=m;
		}

		int get_mode(){	return(this->mode);}

		void set_square(char* s){

			for(int i=0; i<26; i++)
				Square[i]=s[i];
		}

		char* get_square(){		return(Square);}

        void board(){


			cout << "\n\n\tTic Tac Toe\n\n";

			cout << "Player 1 (X)  -  Player 2 (O)" << endl << endl;
			cout << endl;

			cout << "     |     |     |     |     " << endl;
			cout << "   " << Square[1] << "  |  " << Square[2] << "  |  " << Square[3] << "  |  " << Square[4] << "   |  " << Square[5] << endl;

			cout << "_____|_____|_____|_____|_____" << endl;
			cout << "     |     |     |     |          " << endl;

			cout << "     |     |     |     |     " << endl;
			cout << "  " << Square[6] << "  |  " << Square[7] << "  |  " << Square[8] << "  |  " << Square[9] << "   |  " << Square[10] << endl;

			cout << "_____|_____|_____|_____|_____" << endl;
			cout << "     |     |     |     |          " << endl;

			cout << "     |     |     |     |     " << endl;
			cout << "  " << Square[11] << "  |  " << Square[12] << "  |  " << Square[13] << "  |  " << Square[14] << "   |  " << Square[15] << endl;

			cout << "_____|_____|_____|_____|_____" << endl;
			cout << "     |     |     |     |          " << endl;


			cout << "     |     |     |     |     " << endl;
			cout << "  " << Square[16] << "  |  " << Square[17] << "  |  " << Square[18] << "  |  " << Square[19] << "   |  " << Square[20] << endl;

			cout << "_____|_____|_____|_____|_____" << endl;
			cout << "     |     |     |     |          " << endl;


			cout << "     |     |     |     |     " << endl;
			cout << "  " << Square[21] << "  |  " << Square[22] << "  |  " << Square[23] << "  |  " << Square[24] << "   |  " << Square[25] << endl << endl  << endl;
		}


		int checkwin()
		{

			if (Square[1] == Square[2] && Square[2] == Square[3] && Square[3] == Square[4] && Square[4] == Square[5])
					return 1;
			else if (Square[6] == Square[7] && Square[7] == Square[8] && Square[8] == Square[9] && Square[9] == Square[10])
					return 1;
			else if (Square[11] == Square[12] && Square[12] == Square[13] && Square[13] == Square[14] && Square[14] == Square[15])
					return 1;
			else if (Square[16] == Square[17] && Square[17] == Square[18] && Square[18] == Square[19] && Square[19] == Square[20])
					return 1;
			else if (Square[21] == Square[22] && Square[22] == Square[23] && Square[23] == Square[24] && Square[24] == Square[25])
					return 1;
			else if (Square[1] == Square[6] && Square[6] == Square[11] && Square[11] == Square[16] && Square[16] == Square[21])
					return 1;
			else if (Square[2] == Square[7] && Square[7] == Square[12] && Square[12] == Square[17] && Square[17] == Square[22])
					return 1;
			else if (Square[3] == Square[8] && Square[8] == Square[13] && Square[13] == Square[18] && Square[18] == Square[23])
					return 1;
			else if (Square[4] == Square[9] && Square[9] == Square[14] && Square[14] == Square[19] && Square[19] == Square[24])
					return 1;
			else if (Square[5] == Square[10] && Square[10] == Square[15] && Square[15] == Square[20] && Square[20] == Square[25])
					return 1;
			else if (Square[1] == Square[7] && Square[7] == Square[13] && Square[13] == Square[19] && Square[19] == Square[25])
					return 1;
			else if (Square[5] == Square[9] && Square[9] == Square[13] && Square[13] == Square[17] && Square[17] == Square[21])
					return 1;

			int j=0;

			for(int i=1; i<26; i++){

				if(Square[i]=='X' || Square[i]=='O')
					j++;

				if(j==25)
					return 0;

			}

			return -1;
		}
};

#endif 
